Microsoft Windows [Version 5.2.3790]
(C) Copyright 1985-2003 Microsoft Corp.

d:\Experiments\clbuild>python check
python check
python: can't open file 'check': [Errno 2] No such file or directory

d:\Experiments\clbuild>python clbuild check
python clbuild check
  File "clbuild", line 23
    if [ "$CLNET_USER" == "" ]; then
                              ^
SyntaxError: invalid syntax

d:\Experiments\clbuild>cat clbuild
cat clbuild
#!/usr/bin/env bash
# (we use env for non-Linux OSes)
#
# Script to update/install the latest versions of all the most
# important Common Lisp projects. Uses SBCL but otherwise tries to be
# somewhat independent of your local environment.
#
# Intended to quickly bootstrap a working development environment for
# Lisp free software hackers.
#
# Idea from jhbuild by James Henstridge (a Gnome hacker).
#
# Contributors:
#   Luke Gorrie <luke@member.fsf.org>
#   Anthony Chaumas-Pellet <achaumas@wispery.info>
#   Christophe Rhodes <csr21@cantab.net>
#   David Lichteblau <david@lichteblau.com>
#   Eric Marsden <eric.marsden@free.fr>
#   Andreas Fuchs <asf@boinkor.net>

set -e

if [ "$CLNET_USER" == "" ]; then
    CLNET_USER=:pserver:anonymous:anonymous
fi
export CLNET_USER

# MacOS doesn't have "readlink -e", needed to follow symlinks.
# We hack it using readlink, dirname, and cd.
readlink_e() {
    self="$0"
    while test -h "$self"; do
	cd "$(dirname $self)"
	self=`readlink "$self"`
    done
    cd "$(dirname $self)"
    pwd
}
BASE=$(readlink_e)
system_dir="$BASE/systems"
source_dir="$BASE/source"
target_dir="$BASE/target"

if test x`uname -o 2>/dev/null` = xCygwin; then
	windowsp=1
else
	windowsp=""
fi

source "$BASE/clbuild.conf.default"
if test -f "$BASE/clbuild.conf"; then
    source "$BASE/clbuild.conf"
fi
# Fix up pathnames
make_absolute_pn() {
	if [ -n "$1" ] ; then
		(cd "$BASE"
		echo "$(cd "$(dirname "$1")" ; pwd)/$(basename "$1")")
	fi
}
case $UPDATE_SCRIPT in
    update_project)
	# okay, new name
	;;
    update.sh)
	# old name
	UPDATE_SCRIPT=update_project
	;;
    *)
	# do will still want this?
	UPDATE_SCRIPT="$(make_absolute_pn "$UPDATE_SCRIPT")"
	;;
esac
USER_INIT="$(make_absolute_pn "$USER_INIT")"

if test -n "$windowsp" -a x$USER_INIT = x/dev/null; then
	USER_INIT=NUL
fi

# CLIM configuration
case x$CLIM_BACKEND in
    xgraphic-forms|xgtkairo|xbeagle)
        EXTRA_CLIM_FEATURES="(pushnew :clim-$CLIM_BACKEND *features*)"
	maybe_load_clx="nil"
        ;;
    x|xclx)
        EXTRA_CLIM_FEATURES="nil"
	maybe_load_clx="(unless (find-package :xlib) (asdf:operate 'asdf:load-op :clx))"
        ;;
    *)
        echo "invalid $CLIM_BACKEND, see clbuild.conf.default for examples." 1>&2
        exit 1
        ;;
esac

if test -n "$windowsp"; then
	system_namestring="`cygpath -m $system_dir`/"
	source_namestring="`cygpath -m $source_dir`/"
	target_namestring="`cygpath -m $target_dir`/"
	self="c:/cygwin/bin/bash $self"
else
	system_namestring="$system_dir/"
	source_namestring="$source_dir/"
	target_namestring="$target_dir/"
fi
if test -n "$SETF_CENTRAL_REGISTRY"; then
    set_central_registry="(setq asdf:*central-registry* '(#p\"${system_namestring}\"))"
else
    set_central_registry="(push #p\"${system_namestring}\" asdf:*central-registry*)"
fi

configure_ccl() {
    lisp=$1
    noinform="-Q"
    end_toplevel_options="" #fixme
    quit="(ccl:quit)"
    eval="--eval"
    require_asdf="(require :asdf)"
    core_option="-I"

    if test x"$USER_INIT" = x/dev/null; then
	# -l /dev/null does not work
	common_options="-n"
    elif test -n "$USER_INIT"; then
	common_options="-n -l $USER_INIT"
    else
	common_options=""
    fi

    # fixme: this doesn't quite match the SBCL version yet:
    build_options="$noinform --batch $common_options"
    run_options="--batch $common_options"
}

configure_clisp() {
    lisp=$1
    echo '*** Using CLISP.  Please note that CLISP support is not complete.'
    lisp="$CLISP -repl"

    noinform="" #fixme
    end_toplevel_options="" #fixme
    quit="(ext:quit)"
    eval="-x"
    require_asdf="(load \"$BASE/source/asdf-for-clisp/asdf.lisp\")"
    core_option="-M"

    if test -n "$USER_INIT"; then
	common_options="-norc -i $USER_INIT"
    else
	common_options=""
    fi

    build_options="-on-error exit $common_options"
    run_options="-on-error exit $common_options"

    if test -d "$BASE/source/asdf-for-clisp"; then
	echo "*** asdf checkout found"
	echo
    else
	echo "NEW checking out asdf for use with clisp"
	(cd "$BASE/source" && cvs -d ${SF_USER}@sbcl.cvs.sourceforge.net:/cvsroot/sbcl co -d asdf-for-clisp sbcl/contrib/asdf)
    fi
}

configure_sbcl() {
    lisp=$1

    noinform="--noinform"
    end_toplevel_options="--end-toplevel-options"
    quit="(sb-ext:quit)"
    eval="--eval"
    require_asdf="(require :asdf)"
    core_option="--core"

    if test -n "$USER_INIT"; then
	common_options="--userinit $USER_INIT"
    else
	common_options=""
    fi

    build_options="$noinform --noprint --disable-debugger $common_options"
    run_options="--disable-debugger $common_options"
}

if [ ! -z $CCL ]; then
    configure_ccl "$CCL"
elif [ ! -z $CLISP ]; then
    configure_clisp "$CLISP"
elif [ ! -z "$SBCL" ]; then
    configure_sbcl "$SBCL"
elif [ -x ${target_dir}/bin/sbcl ]; then
    export SBCL_HOME=${target_namestring}lib/sbcl/
    lisp="${target_dir}/bin/sbcl"
    if ! test -f "$BASE"/monster.core; then
	lisp="$lisp --core ${target_namestring}lib/sbcl/sbcl.core"
    fi
    configure_sbcl "$lisp"
elif [ -x ${source_dir}/ccl/lx86cl64 ]; then
    configure_ccl ${source_dir}/ccl/lx86cl64
else
    configure_sbcl sbcl
fi

[ -d "$system_dir" ] || mkdir "$system_dir"
[ -d "$source_dir" ] || mkdir "$source_dir"
[ -d "$target_dir" ] || mkdir "$target_dir"

clbuild_lisp() {
    ${lisp} $common_options \
	$eval "$require_asdf" \
	$eval "$set_central_registry" \
	$eval "$EXTRA_CLIM_FEATURES" \
	"$@"
}

lisp_trampoline() {
    # Start the Lisp with user arguments.  For SBCL, we can do that using
    # --end-toplevel-options.  For other Lisps, go through a temporary
    # file.

    options="$1"
    shift

    if test -n "$end_toplevel_options"; then
	${lisp} \
	    $options \
	    $eval "$require_asdf" \
	    $eval "$set_central_registry" \
	    $eval "$EXTRA_CLIM_FEATURES" \
	    $eval "(load \"$BASE/clbuild.lisp\")" \
	    $end_toplevel_options \
	    "$@"
    else
        TMPDIR=`mktemp -d /tmp/clbuild.XXXXXXXXXX`
	export TMPDIR

        cleanup() {
            rm -rf $TMPDIR
        }
        trap cleanup exit

	while test -n "$1"; do
	    # fixme: whitespacea
	    echo $1 >>$TMPDIR/args
	    shift
	done

	${lisp} \
	    $options \
	    $eval "$require_asdf" \
	    $eval "$set_central_registry" \
	    $eval "$EXTRA_CLIM_FEATURES" \
	    $eval "(defparameter cl-user::*clbuild-args* \"$TMPDIR/args\")" \
	    $eval "(load \"$BASE/clbuild.lisp\")"
    fi
}

start_application() {
    lisp_trampoline "$noinform $run_options" "$@"
}

recompile() {
	if test x"$1" = x--dump; then
	    dump="--dump t"
	    shift
	else
	    dump=""
	fi

	concatenated_args="$@"
	if test -z "$concatenated_args"; then
	    concatenated_args="$main_projects"
	fi

	cd "$BASE"
	lisp_trampoline "$build_options" \
	    recompile-systems \
	    $dump \
	    "$concatenated_args"
}

count_systems() {
	n_asd=`ls -1 "$system_dir"/*.asd | wc -l`
	echo "$n_asd system definition files registered"
}

blank_line="                                                                  "
tail_last() {
    if tty 0>&1 >/dev/null; then
	while read line; do
	    echo -e '\r\c'
	    echo -n "$blank_line"
	    echo -e '\r\c'
	    echo -n $line | cut -b 1-65 | tr -d '\n'
	done
	echo -e '\r\c'
	echo -n "$blank_line"
	echo -e '\r\c'
    else
	while read line; do
	    echo $line
	done
    fi
}

dribble_get() {
    label="$1"
    name="$2"

    if test -d `echo ${name}*/ | awk '{print $1;}'`; then
	echo -n "UPDATE "
    else
	echo -n "NEW "
    fi
    echo "$label $name"
}

dry_run_ok() {
   if test -n "$dry_run"; then
       echo "OK: $1"
   fi
}

dry_run_missing() {
   if test -n "$dry_run"; then
       echo "MISSING: $1"
   fi
}

get_darcs() {
    name="$1"
    url="$2"

    if [ -d $name ]; then
	actual="`cat $name/_darcs/prefs/defaultrepo`"
	if test "x$actual" = "x$url"; then
	    dry_run_ok $1
	else
	    echo "MISMATCH: $1 was installed from $actual, current is $url"
	fi
    else
	dry_run_missing $1
    fi
    if test -n "$dry_run"; then
	exit 0
    fi

    # don't use tail_last, since darcs already has this kind of progress bar
    if [ -d $name ]; then
	dribble_get "darcs pull" $name
	(
	    cd $name
	    if ! test -d _darcs; then
		echo ERROR: not a darcs repository
		exit 1
	    fi
	    darcs pull --all
	    )
    else
	dribble_get "darcs get" $name
	darcs get $url $name
    fi
}

get_git() {
    name="$1"
    url="$2"

    if [ -d $name ]; then
	actual="`cd $name && git config --get remote.origin.url`"
	if test "x$actual" = "x$url"; then
	    dry_run_ok $1
	else
	    echo "MISMATCH: $1 was installed from $actual, current is $url"
	fi
    else
	dry_run_missing $1
    fi
    if test -n "$dry_run"; then
	exit 0
    fi

    if [ -d $name ]; then
	dribble_get "git pull" $name
	(
	    cd $name
	    if ! test -d .git; then
		echo ERROR: not a git repository
		exit 1
	    fi
	    git pull
	    )
    else
	dribble_get "git clone" $name
	git clone $url $name
    fi
}

get_svn() {
    name="$1"
    url="$2"

    if [ -d $name ]; then
	actual="`cd $name && svn info | grep ^URL: | awk '{print $2;}'`"
	if test "x$actual" = "x$url"; then
	    dry_run_ok $1
	else
	    echo "MISMATCH: $1 was installed from $actual, current is $url"
	fi
    else
	dry_run_missing $1
    fi
    if test -n "$dry_run"; then
	exit 0
    fi

    dribble_get "svn co" $name

    svn co $url $name | tail_last
}

get_cvs_aux() {
    module="$1"
    repository="$2"
    target_directory="$3"

    if [ -d $module ]; then
	actual="`cat $module/CVS/Root`"
	if test "x$actual" = "x$repository"; then
	    dry_run_ok $1
	else
	    echo "MISMATCH: $1 was installed from $actual, current is $repository"
	fi
    else
	dry_run_missing $1
    fi
    if test -n "$dry_run"; then
	exit 0
    fi

    dribble_get "cvs co" $module

    cvs -d $repository co ${3+-d "$3"} $module | tail_last
}

get_cvs_full() {
    get_cvs_aux $3 $2 $1
}

get_tarball() {
    name="$1"
    url="$2"
    flags="${3:-z}"

    echo "Warning: Using deprecated method get_tarball."

    if test -n "$dry_run"; then
	if ls -d ${name}* >/dev/null; then
	    directories="`ls -d ${name}*`"
	    echo "TARBALL: $directories installed from a tarball, cannot check"
	else
	    dry_run_missing $1
	fi
	exit 0
    fi

    tmp=$TMPDIR/${name}.tar.gz

    dribble_get wget $name

    # We used to delete old directories completely here.  Unfortunately,
    # that can easily lead to loss of changes, a problem all the VC checkout
    # method do not have to the extent.  Save all old data instead.  Users
    # can delete the resulting backup directories easily enough themselves.
    if ls ${name}* >/dev/null; then
	echo got here
	backup=`mktemp -d backup_${name}_XXXXXXXXXX`
	mv ${name}* "$backup/"
    fi
    wget \
	--no-check-certificate \
	--progress=dot \
	-O "$tmp" \
	$url \
	2>&1 | tail_last
    tar v${flags}xf "$tmp" | tail_last
    rm "$tmp"
}

get_svn_clnet() {
    name="$1"
    path="$2"

    get_svn $name svn://common-lisp.net/project/$name/svn/$2
}

get_cvs_clnet() {
    module="$1"
    project="${2:-$1}"

    get_cvs_aux $module ${CLNET_USER}@common-lisp.net:/project/$project/cvsroot
}

get_cvs_clnet_full() {
    clbuildproject="$1"
    clnetproject="${2:-$1}"
    path="$3"

    get_cvs_aux $path ${CLNET_USER}@common-lisp.net:/project/$clnetproject/cvsroot $clbuildproject
}

get_cvs_sfnet() {
    module="$1"
    project="${2:-$1}"

    get_cvs_aux $module ${SF_USER}@$project.cvs.sourceforge.net:/cvsroot/$project
}

get_ediware() {
    get_darcs $1 http://common-lisp.net/~loliveira/ediware/$1
}

get_clbuild_mirror() {
    get_darcs $1 http://common-lisp.net/project/clbuild/mirror/$1
}

get_lichteblau_com() {
    get_git $1 http://www.lichteblau.com/git/$1.git
}

get_b9_com() {
    get_git $1 http://git.b9.com/$1.git
}

get_xach_com() {
    get_git $1 http://git.xach.com/$1.git
}

get_tarball_bz2() {
    get_tarball "$1" "$2" j
}

update_project() {
    if test x$1 = x--dry-run; then
	shift
	dry_run=1
    else
	unset dry_run
    fi
    export dry_run

    if test $# -ne 1; then
	exec 1>&2
	echo error: invalid number of arguments
	echo usage: ... update [--dry-run] PROJECT_NAME
	exit 1
    fi

    if ! grep -h "^$1 " "$BASE/projects" "$BASE/wnpp-projects" "$BASE/implementations" >/dev/null; then
       echo Error: cannot download unknown project $1
       rm "$BASE/.clbuild-resume"
       exit 1
    fi
    found=`grep -h "^$1 " "$BASE/projects" "$BASE/wnpp-projects" "$BASE/implementations" | cut -d\# -f1`
    update_project_2 $found
}

update_project_2() {
    name="$1"
    action="$2"
    shift
    shift
    ( $action $name "$@" )
}

update() {
    if test -n "$clbuild_resume"; then
	if test $# -gt 0; then
	    echo "error: --resume conflicts with arguments" 1>&2
	    exit 1
	fi
	previous=`cat "$BASE/.clbuild-resume"`
	CLBUILD_DEPENDENCIES=no update_1 $previous
    elif test $# -gt 0; then
	rm -f $source_dir/*/.clbuild-skip-update
	update_1 $*
    else
	exec 1>&2
	echo "Error: arguments expected"
	echo "Usage:"
	echo "  clbuild update PROJECT1 PROJECT2 PROJECT3...  # only these"
	echo "  clbuild update --resume            # resume interrupted update"
	echo "  clbuild update --main-projects     # all in projects file"
	echo "  clbuild update --wnpp-projects     # all in wnpp-projects file"
	echo "  clbuild update --all-projects      # both files"
	echo "  clbuild update --installed         # everything in source/"
	exit 1
    fi
}

update_1() {
        touch "$BASE"/.core-is-stale

        TMPDIR=`mktemp -d /tmp/clbuild.XXXXXXXXXX`
	export TMPDIR

        cleanup() {
	    if test -f "$BASE/.clbuild-resume"; then
		exec 1>&2
		echo
		echo error: update was interrupted.
		echo 'Use "clbuild update --resume" to retry.  (See also "clbuild skip PROJECT").'
	    fi
            rm -rf $TMPDIR
        }
        trap cleanup exit

	cd "$source_dir"

	touch $TMPDIR/dependencies0
	while test $# -ge 1; do
	    echo $1 >>$TMPDIR/arguments0
	    if grep "^$1 " ../dependencies >/dev/null; then
		found=`grep "^$1 " ../dependencies`
		for x in $found; do
		    echo $x >>$TMPDIR/dependencies0
		done
	    else
		echo $1 >>$TMPDIR/dependencies0
		echo "warning: no dependencies for $1 found" 1>&2
	    fi
	    shift
	done
	sort <$TMPDIR/arguments0 | uniq >$TMPDIR/arguments
	sort <$TMPDIR/dependencies0 | uniq >$TMPDIR/dependencies

	if ! cmp $TMPDIR/arguments $TMPDIR/dependencies >/dev/null; then
	    case "$CLBUILD_DEPENDENCIES" in
		ask)
		    extra=`diff $TMPDIR/arguments $TMPDIR/dependencies | grep '^>' | cut -d' ' -f2 | xargs echo`
		    echo "The following extra dependencies were found: $extra"
		    echo -n "include dependencies in update? (Y/n)"
		    read reply
		    case _"$reply" in
			_Y|_y|_)
			    ;;
			_n)
			    mv $TMPDIR/arguments $TMPDIR/dependencies
			    ;;
			*)
			    echo Invalid reply
			    exit 1
			    ;;
		    esac
		    ;;
		yes)
		    ;;
		no)
		    mv $TMPDIR/arguments $TMPDIR/dependencies
		    ;;
		*)
		    echo "error: invalid \$CLBUILD_DEPENDENCIES" 1>&2
		    exit 1
	    esac
	fi

	cp $TMPDIR/dependencies "$BASE/.clbuild-resume"

	for project in $(cat $TMPDIR/dependencies); do
	    skipfile="$project"/.clbuild-skip-update
	    if test -f "$skipfile" -a -n "$clbuild_resume"; then
		echo "resume: skipping update of $project"
	    else
		${UPDATE_SCRIPT} ${UPDATE_ARGS} $project
		ln -f -s $(pwd)/${project}*/*.asd "${system_dir}"/
		touch "$skipfile"
	    fi
	done

	link_extra_asds

	rm "$BASE/.clbuild-resume"

	echo "update complete"
	count_systems
        cd ..
}

link_extra_asds() {
    # some (buggy) projects try to hide their .asd files from us:
    ln -sf $source_dir/mcclim/Experimental/freetype/*.asd ${system_dir}/
    ln -sf $source_dir/eclipse/system.lisp ${system_dir}/eclipse.asd
    ln -f -s $source_dir/graphic-forms/src/external-libraries/*/*/*.asd \
	${system_dir}
    ln -sf $source_dir/clg/*/*.asd ${system_dir}
    ln -sf $source_dir/cells-gtk/*/*.asd ${system_dir}

    # also, override uffi:
    ln -sf $source_dir/cffi/uffi-compat/uffi.asd ${system_dir}/
}

help() {
	cat <<EOF
Usage:
  check         check availability of all necessary helper applications

  list [PATTERN]  list all projects, or projects matching PATTERN

  update [--dependencies|--no-dependencies] PROJECT_SPEC
                download/update this project
  update [--resume]
                download/update main projects.  With --resume, consider
                only projects that a previous update run failed to fetch.
  skip PROJECT_NAME
                mark this project as done for the purposes of update --resume

  recompile PROJECT_SPEC compile fasls
  dumpcore PROJECT_SPEC  recompile and dump a core file for faster startup
  build PROJECT_SPEC     update && dumpcore

  diff          show local changes (for all version-controlled projects)
  check-urls    compared installed repository urls agains current locations
  clean-links   remove broken symlinks in systems/
  update-missing  download only projects not present yet
  register-asd PROJECT  add .asd file symlinks for PROJECT

  compile-implementation sbcl [XC_HOST]  compile SBCL
  compile-implementation ccl             compile Clozure CL 
  compile-implementation clisp           compile CLISP

  world         build sbcl and specified projects

  clean [PROJECT]  delete all compiled object files [in source/PROJECT]
  trash PROJECT    move source/PROJECT to trash/
  mrproper         trash all projects

  slime         run the Superior Lisp Interaction Mode in a fresh Emacs
  lisp          run Lisp in the terminal (using sbcl.core)
  preloaded     run Lisp in the terminal (using monster.core)
  slime-configuration  print .emacs excerpt for slime

  record-dependencies   rebuild dependency information file

For update/recompile/build/dumpcore a PROJECT_SPEC can be used:
  PROJECT1 PROJECT2 PROJECT3...  # only these
  --main-projects                # all in projects file
  --wnpp-projects                # all in wnpp-projects file
  --all-projects                 # both files
  --installed                    # everything in source/

Starting applications:

  run APPLICATION   run this application
  run --help        show help for applications

If you do 'world' or 'buildsbcl' then SBCL will be installed in
target/ and used for future commands. If you don't run these commands
(or you remove target/) then clbuild uses the 'sbcl' in your PATH.

For configuration options (including for non-SBCL lisps), see clbuild.conf.

EOF
}

help_run() {
	cat <<EOF
clbuild run APPLICATION [ARGS...]

  run listener      run the McCLIM listener
  run gsharp        run the Gsharp score editor 
  run climacs       run the Climacs text editor
  run closure [HOME_PAGE_URL]   run the CLOSURE web browser
                    (required Debian packages: gif2png,libjpeg-progs)
  run beirc         run the Beirc IRC client
  run climplayer    run the CLIMPlayer music player
                    (required Debian packages: mplayer, fileschanged, fam)
  run demodemo      run some random CLIM examples
  run clim-alerts   run CLIM alerts
  run eclipse [DPY] run the eclipse window manager

  run hunchentoot   run the Hunchentoot web server test
  run webdav DIR    run the CL-WEBDAV server, serving directory DIR
                    (required Debian packages: libssl-dev)

  run parse-xml FILENAME
                    check XML for well-formedness
  run validate-xml FILENAME
                    check XML for validity
  run validate-relax-ng [--compact yes] XML-FILENAME SCHEMA-FILENAME
                    check XML for validity against a Relax NG Schema
  run html-to-xhtml HTML-FILENAME OUTPUT-FILENAME
  run xhtml-to-html XML-FILENAME OUTPUT-FILENAME
                convert between HTML 4 and XHTMl

  run vecto-demo         generate a test image using vecto
  run adw-charting-demo  generate a test image using adw-charting

  run ltk-demo      show a dialog using ltk
EOF
}

check_program() {
    if ! "$1" --help >/dev/null; then
	echo Error: Cannot find a working installation of "$1"
	exit 1
    fi
    echo "found `which $1`"
}

# for programs that don't understand --help, or (like cvs) are stupid enough
# to return a failure code when invoked using a correct --help option... 
check_misdesigned_program() {
    if ! which "$1" >/dev/null; then
	echo Error: Cannot find an installation of "$1" at all
	exit 1
    fi
    echo "found `which $1`"
}

check() {
    echo "Checking for helper applications..."
    check_misdesigned_program cvs
    check_program svn
    check_program darcs
    check_program wget
    # get_tarball is evil and unused anyway, so no need to bother the FreeBSD
    # users with warnings about their non-GNU version of tar
#    check_program tar
    check_misdesigned_program curl
    check_misdesigned_program git
    check_misdesigned_program mktemp
    echo "Success: All helper applications found."
	
    echo
    echo "Checking Lisp startup..."
    if ${lisp} $run_options $eval $quit >/dev/null; then
	echo "Success: Lisp starts up using \"$lisp\""
    else
	echo "Error: Cannot run Lisp using \"$lisp\""
	exit 1
    fi

    echo
    echo "Looking for installable systems..."
    count_systems
}

dumpcore() {
    rm -f "$BASE"/.core-is-stale
    rm -f "$BASE"/monster.core
    recompile --dump "$@"
}

if test -f "$BASE"/monster.core; then
    # Initially I thought this warning might be helpful, but it's usually
    # false alarm:
#     if test -f "$BASE/.core-is-stale"; then
# 	echo "using $BASE/monster.core"
# 	echo 'note: "clbuild update" was used since core file creation'
# 	echo 'note: consider re-running dumpcore'
#     fi
    run_options="$core_option "$BASE"/monster.core $run_options"
fi

scan_projects() {
    cat $1 | awk '{print $1;}' | while read name; do
	if test -n "$name" -a x"$name" != 'x#'; then
	    echo -n "$name "
	fi
    done
}

scan_installed() {
    for f in ${source_dir}/*; do
	name=`basename "$f"`
	if test -d "$f"; then
	    if test -d "$f"/_darcs \
		-o -d "$f"/.git \
		-o -d "$f"/.svn \
		-o -d "$f"/CVS \
		&& grep "^$name " "$BASE/dependencies" >/dev/null
	    then
		echo $name
	    else
		echo "skipping $f" 1>&2
	    fi
	fi
    done
}

main_projects=`scan_projects "$BASE/projects"`
wnpp_projects=`scan_projects "$BASE/wnpp-projects"`
all_projects="$main_projects $wnpp_projects"

implementations=`scan_projects "$BASE/implementations"`

set_installed_projects() {
    installed_projects=`scan_installed`
}

list() {
    pattern="$1"

    TMPDIR=`mktemp -d /tmp/clbuild.XXXXXXXXXX`
    export TMPDIR
    
    cleanup() {
        rm -rf $TMPDIR
    }
    trap cleanup exit

    cat "$BASE/projects" "$BASE/wnpp-projects" "$BASE/implementations" | sort | grep -i -E "$pattern" | while read project rest; do
	if test -n "$project" -a x"$project" != 'x#'; then
	    description=`echo $rest | cut -d\# -f2`
	    case `cd $source_dir && ${UPDATE_SCRIPT} --dry-run $project | cut -d: -f1` in
		MISSING)
		    status=u
		    ;;
		MISMATCH)
		    status="!"
		    ;;
		OK)
		    status="i"
		    ;;
		*)
		    echo "failed to check URL" 1>&2
		    ;;
	    esac

	    echo "$status $project" >>$TMPDIR/left
	    echo $description >>$TMPDIR/right
	fi
    done
    paste $TMPDIR/left $TMPDIR/right | expand -t 25
    exit 0
}

ensure_clppcre() {
    if ! test -d "$source_dir/cl-ppcre"; then
	(
	    cd "$source_dir"
	    update_project cl-ppcre
	    ln -f -s $source_dir/cl-ppcre/*.asd "${system_dir}"/
	)
    fi
}

write_slime_configuration() {
    if test -n "$START_SLIME_USING_CORE"; then
	cmd=preloaded
    else
	cmd=lisp
    fi
    cat <<EOF

;; possibly controversial as a global default, but shipping a lisp
;; that dies trying to talk to slime is stupid, so:
(set-language-environment "UTF-8")
(setq slime-net-coding-system 'utf-8-unix)

;; load slime:
(setq load-path (cons "${source_namestring}slime" load-path))
(setq load-path (cons "${source_namestring}slime/contrib" load-path))
(setq slime-backend "$BASE/.swank-loader.lisp")
(setq inhibit-splash-screen t)
(load "${source_namestring}slime/slime")
(setq inferior-lisp-program "$BASE/clbuild $cmd")
(setq slime-use-autodoc-mode nil)
(slime-setup '(slime-fancy slime-tramp slime-asdf))
(slime)
EOF
    # while we're at it, also write the swank loader
    cat >$BASE/.swank-loader.lisp <<EOF
(unless (find-package 'swank-loader)
  (load "$source_dir/slime/swank-loader.lisp"))
EOF
}

trash() {
    mkdir -p "$BASE/trash"
    basename=`basename $1`
    today=`date +'%Y-%m-%d'`
    trash="$BASE/trash/$today"
    if test -e "$trash"; then
	if test -e "$trash/$basename"; then
	    trash=`mktemp -d $BASE/trash/${today}_${basename}_XXXXXXXXXX`
	fi
    else
	mkdir $trash
    fi
    echo moving "$1" to "$trash/$basename"
    mv "$1" "$trash"
}

case $1 in
    check)
	check
	;;
    list)
	list "${2:-.*}"
	;;
    clean)
	clean_fasls() {
	    echo cleaning fasls in `pwd`
	    find . -name "*.fasl" -exec rm {} \;
	    find . -name "*.lx64fsl" -exec rm {} \;
	}
	if test $# -gt 1; then
	    while test $# -gt 1; do
		( cd "$source_dir/$2" && clean_fasls )
		shift
	    done
	else
	    core="$BASE"/monster.core
	    if test -f "$core"; then
		echo removing $core
		rm -f "$core"
	    fi
	    cd $source_dir
	    clean_fasls
	fi
	;;
    trash)
	if test $# -le 1; then
	    echo 'usage: trash [PROJECT...]'
	    exit 1
	fi
	while test $# -gt 1; do
	    d="$source_dir/$2"
	    if test -d "$d"; then
		trash "$d"
	    else
		echo "cannot trash non-existing directory $d"
	    fi
	    shift
	done
	;;
    mrproper)
	for d in $source_dir/* $target_dir; do
	    if test -d "$d"; then
		trash "$d"
	    else
		echo "skipping $d"
	    fi
	done
	echo "deleting monster.core systems/*"
	rm -f "$BASE"/monster.core ${system_dir}/*
	;;
    skip)
	touch "$source_dir/$2/.clbuild-skip-update"
	;;
    update)
	unset clbuild_resume
	unset extra
	while test -n "$2"; do
	    case "$2" in
		--no-dependencies)
		    CLBUILD_DEPENDENCIES="no"
		    shift
		    ;;
		--dependencies)
		    CLBUILD_DEPENDENCIES="yes"
		    shift
		    ;;
		--resume)
		    clbuild_resume="yes"
		    shift
		    ;;
		--main-projects)
		    extra="$extra $main_projects"
		    shift
		    ;;
		--wnpp-projects)
		    extra="$extra $wnpp_projects"
		    shift
		    ;;
		--all-projects)
		    extra="$extra $all_projects"
		    shift
		    ;;
		--installed)
		    set_installed_projects
		    extra="$extra $installed_projects"
		    shift
		    ;;
		*)
		    break
		    ;;
	    esac
	done
	export clbuild_resume
	export CLBUILD_DEPENDENCIES
	shift
	update "$@" $extra
	;;
    update-missing)
	for project in ${2:-$main_projects}; do
	    if ! test -d "${source_dir}/$project"; then
		update $project
	    fi
	done
	;;
    recompile|build|dumpcore|world)
	operation="$1"
	shift
	unset projects
	while test -n "$1"; do
	    case "$1" in
		--main-projects)
		    projects="$projects $main_projects"
		    shift
		    ;;
		--wnpp-projects)
		    projects="$projects $wnpp_projects"
		    shift
		    ;;
		--all-projects)
		    projects="$projects $all_projects"
		    shift
		    ;;
		--installed)
		    CLBUILD_DEPENDENCIES="yes"
		    set_installed_projects
		    projects="$projects $installed_projects"
		    shift
		    ;;
		*)
		    break
		    ;;
	    esac
	done
	if test -z "$projects" -a $# -eq 0; then
	    exec 1>&2
	    echo "Error: arguments expected"
	    echo "Usage:"
	    echo "  clbuild $operation PROJECT1 PROJECT2 PROJECT3...  # only these"
	    echo "  clbuild $operation --main-projects     # all in projects file"
	    echo "  clbuild $operation --wnpp-projects     # all in wnpp-projects file"
	    echo "  clbuild $operation --all-projects      # both files"
	    echo "  clbuild $operation --installed         # everything in source/"
	    exit 1
	fi
	ensure_clppcre
	case $operation in
	    recompile)
		recompile "$@" $projects
		;;
	    build)
		update "$@" $projects
		dumpcore "$@" $projects
		;;
	    dumpcore)
		dumpcore "$@" $projects
		;;
	    world)
		"$0" clean && "$0" update sbcl && "$0" compile-implementation sbcl && "$0" build "$@" $projects
		;;
	esac
	;;
    updatesbcl)
	exec 1>&2
	echo "error: invalid command updatesbcl"
	echo "Use 'clbuild update sbcl' instead."
	exit 1
	;;
    buildsbcl)
	exec 1>&2
	echo "error: invalid command buildsbcl"
	echo "Use 'clbuild compile-implementation sbcl' instead."
	exit 1
	;;
    compile-implementation)
	case $2 in
	    sbcl)
		if [ -n "$CCL" ]; then
		    echo "Cowardly refusing to build SBCL when \$CCL is set." 1>&2
		    exit 1
		fi
		if ! test -d ${source_dir}/sbcl; then
		    echo "sbcl not found, try running 'clbuild update sbcl'" 1>&2
		    exit 1
		fi
                # Enable threads
		if test -z "$windowsp"; then
		    ctf=$source_dir/sbcl/customize-target-features.lisp
		    if test -f $ctf; then
			echo $ctf already exists
		    else
			echo creating $ctf
			cat >$ctf <<EOF
(lambda (list)
  (pushnew :sb-thread list)
  list)
EOF
		    fi
		fi
		(cd $source_dir/sbcl; sh make.sh "$3"; SBCL_HOME= INSTALL_ROOT=${target_dir} sh install.sh)
		;;
	    ccl)
		if [ -z "$CCL" ]; then
		    echo "can't build CCL unless \$CCL is set." 1>&2
		    exit 1
		fi
		if ! test -d ${source_dir}/ccl; then
		    echo "ccl not found, try running 'clbuild update ccl'" 1>&2
		    exit 1
		fi
		ccl_old=`dirname "$CCL"`
		ccl_new=${source_dir}/ccl
		case `uname` in
		    Linux)
			case `uname -m` in
			    x86_64)
				headerdir=x86-headers64
				binary=lx86cl64
				image=LX86CL64
				;;
			    *)
				echo "don't know how to build CCL for your ISA, please adjust clbuild as needed" 2>&1
				exit 1
				;;
			esac
			;;
		    *)
			echo "don't know how to build CCL for your OS, please adjust clbuild as needed" 2>&1
			exit 1
			;;
		esac
		if ! test -f "$ccl_old/$image"; then
		    echo "can't find existing CCL installation in $ccl_old" 2>&1
		    exit 1
		fi
		# cp $ccl_old/$headerdir/libc/*.cdb $ccl_new/$headerdir/libc/
		cp -r $ccl_old/$headerdir $ccl_new/ || true
		cp $ccl_old/$binary $ccl_old/$image $ccl_new/
		cd $ccl_new
		chmod +x $binary
		./$binary -e '(rebuild-ccl :force t)'
		cat <<EOF


Done compiling CCL.
Set this in clbuild.conf to enable the new CCL:
  CCL=$ccl_old/$binary
EOF
		;;
	    clisp)
		if ! test -d ${source_dir}/clisp; then
		    echo "clisp not found, try running 'clbuild update clisp'" 1>&2
		    exit 1
		fi
		cd $source_dir/clisp
		./autogen.sh
		ulimit -s 16384
		rm -f src/config.cache
		./configure --with-libsigsegv-prefix=/usr
		cd src
		make
		;;
	    *)
		echo "unrecognized argument.  valid are: sbcl ccl clisp" 2>&1
		exit 1
		;;
	esac
	;;
    lisp)
	shift;
        clbuild_lisp "$@"
        ;;
    preloaded)
	shift;
	if test -f "$BASE"/monster.core; then
	    common_options="$core_option "$BASE"/monster.core $common_options"
	fi
        clbuild_lisp "$@"
        ;;
    slime)
	shift
	emacs_args="$@"
	emacs=${EMACS-emacs}
	write_slime_configuration >"$BASE/.start-slime.el"
	$emacs -l "$BASE/.start-slime.el" ${emacs_args}
	;;
    slime-configuration)
	shift
	echo ';; add this to your ~/.emacs to use clbuild and its slime:'
	echo ';;'
	write_slime_configuration
	;;
    run)
	shift
	case $1 in
	    --help)
		help_run
		exit 0
		;;
	    climplayer)
		check_program fileschanged
		check_misdesigned_program mplayer
		;;
	    hunchentoot|webdav)
		if ! test -f /usr/lib/libssl.so; then
		    echo "WARNING: /usr/lib/libssl.so not found, did you install OpenSSL?"
		    echo "(type RET to continue anyway)"
		    read
		fi
		;;
	    perfectstorm)
		if ! test -f /usr/include/GL/glut.h; then
		    echo "WARNING: /usr/include/GL/glut.h not found, did you install OpenGL libraries?"
		    echo "(type RET to continue anyway)"
		    read
		fi
		;;
	esac
	start_application "$@"
	;;
    record-dependencies)
	cd "$BASE"
	for project in $all_projects; do
	    if ! test -d "${source_dir}/$project"; then
		update $project
	    fi
	done
	# hack: don't want the core here
	run_options="$build_options"
	start_application record-dependencies "$all_projects"
	;;
    check-urls)
	cd "$source_dir"
	for project in ${2:-$all_projects}; do
	    ${UPDATE_SCRIPT} --dry-run $project
	done
	;;
    clean-links)
	cd "$system_dir"
	for f in *; do
	    if test -h "$f"; then
		link_target=`readlink "$f"`
		if ! test -e "$link_target"; then
		    echo "removing broken link from $f to $link_target"
		    rm "$f"
		fi
	    fi
	done
	;;
    register-asd)
	project="$2"
	dir="$source_dir/$project"

	if test -z "$project"; then
	    echo "usage: clbuild register-asd PROJECT" 1>&2
	    exit 1
	fi
	if ! test -d "$dir"; then
	    echo "cannot find $dir" 1>&2
	    exit 1
	fi
	for f in $dir/*.asd; do
	    if test -f "$f"; then
		echo "$f"
		ln -f -s $f "${system_dir}"/
	    fi
	done
	link_extra_asds
	count_systems
	;;
    diff)
	diff="${source_dir}/.diff"
	cp /dev/null "$diff"
	cd "${source_dir}"
	darcs diff -u >>"$diff"
	set +e
	for f in *; do
	    g="${source_dir}/$f"
	    if test -d "$g"; then
		cd "$g"
		echo -n diffing $f... 1>&2
		if test -d CVS; then
		    cvs diff -u 2>/dev/null | grep -v '^?' >>"$diff"
		elif test -d .svn; then
		    svn diff >>"$diff"
		elif test -d '{arch}'; then
		    baz diff >>"$diff"
		elif test -d _darcs; then
		    darcs diff -u >>"$diff"
		elif test -d .git; then
		    git diff >>"$diff"
		else
		    echo -n " FAILED" 1>&2
		fi
		echo 1>&2
	    fi
	done
	less "$diff"
	;;
    help|--long-help|-H|""|--help|-h)
	case $2 in
	    run)
		help_run
		;;
	    *)
		help
	esac
	;;
    *)
	echo "invalid command $1, try --help for help"
	exit 1
esac

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check
  File "clbuild.py", line 23
    exincept OSError:
                   ^
SyntaxError: invalid syntax

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check
  File "clbuild.py", line 147
    print >>asdffile, r"#+clisp (dolist (dir-candidate (directory (concatenate 'string *lisp-dirs* \"*/\"))) (let ((asd-candidate (merge-pathnames \"*.asd\" dir-candidate))) (when (directory asd-candidate) (push dir-candidate asdf:*central-registry*))))")
                                                                                                                                                                                                                                                              ^
SyntaxError: invalid syntax

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check
  File "clbuild.py", line 193
    getopt.getopt(sys.argv[1:], "", [ "setup=", "check" ]])]
                                                         ^
SyntaxError: invalid syntax

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check
  File "clbuild.py", line 193
    getopt.getopt(sys.argv[1:], "", [ "setup=", "check" ]])
                                                         ^
SyntaxError: invalid syntax

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check
Traceback (most recent call last):
  File "clbuild.py", line 5, in <module>
    import pat
ImportError: No module named pat

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check
Traceback (most recent call last):
  File "clbuild.py", line 181, in <module>
    if (lisp == "clisp"):
NameError: name 'lisp' is not defined

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check
  File "clbuild.py", line 184
    def main()
             ^
SyntaxError: invalid syntax

d:\Experiments\clbuild>python clbuild.py check
python clbuild.py check

d:\Experiments\clbuild>python clbuild.py --check
python clbuild.py --check
CVS Found and present.
SVN Found and present.
Darcs found and present
GIT Found and present.

d:\Experiments\clbuild> cd ..
 cd ..

D:\Experiments>